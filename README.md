## About


sbo-create, it's a tool that creates easy, fast and safe SlackBuilds files scripts.

This tool is for everyone, but maintainers will be going to love it!

Enjoy!


## Features


- Preloaded SlackBuilds templates.
- Checking for already SlackBuilds in the repository and the distribution.
- Autocorrect the quote marks for the .info file.
- Auto-importing the SlackBuild script name.
- Auto-importing the text from the slack-desc file into the README.
- Auto-importing the maintainer data to the .SlackBuild script.
- Auto-importing the version to the .SlackBuild script.
- Auto-importing and checking the checksum signature to the .info file.
- Auto-create all the necessary files for your SlackBuild package.

## Documentation


https://dslackw.gitlab.io/sbo-create
